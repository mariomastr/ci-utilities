#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Julius Künzel <jk.kdedev@smartlab.uber.space>
# SPDX-License-Identifier: BSD-2-Clause

import subprocess
import sys
import os


if __name__ == '__main__':

    try:
        destination = sys.argv[1]
    except ValueError:
        print("usage: {} destination".format(sys.argv[0]))
        sys.exit(1)

    orignalCWD = os.getcwd()

    os.makedirs(destination, exist_ok=False)

    os.chdir(destination)
    subprocess.check_call(["git", "init"])

    repoUrl = os.environ["CI_REPOSITORY_URL"]

    refName = os.environ['CI_COMMIT_REF_NAME']

    if "CI_MERGE_REQUEST_REF_PATH" in os.environ:
        refPath = os.environ["CI_MERGE_REQUEST_REF_PATH"]
        # shortRef = f"merge-request/{mrIID}"
        if "CI_MERGE_REQUEST_PROJECT_URL" in os.environ:
            repoUrl = os.environ["CI_MERGE_REQUEST_PROJECT_URL"] + ".git"
    else:
        refPath = os.environ['CI_COMMIT_REF_NAME']

    subprocess.check_call(["git", "remote", "add", "origin", repoUrl])

    subprocess.check_call(["git", "fetch", "origin", f"+{refPath}:refs/remotes/origin/{refName}", "--depth=50"])

    subprocess.check_call(["git", "checkout", refName])

    # Finally checkout submodules if any
    if os.path.exists(".gitmodules"):
        subprocess.check_call(["git", "submodule", "update", "--init", "--recursive"])

    os.chdir(orignalCWD)
